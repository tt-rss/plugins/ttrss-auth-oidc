<?php
require_once __DIR__ . "/vendor/autoload.php";

use Jumbojett\OpenIDConnectClient;

class Auth_OIDC extends Auth_Base {

	/** redirect user to this URL after logout; .env:
	 * TTRSS_AUTH_OIDC_POST_LOGOUT_URL=http://127.0.0.1/logout-redirect
	 */
	const AUTH_OIDC_POST_LOGOUT_URL = "AUTH_OIDC_POST_LOGOUT_URL";
	const AUTH_OIDC_NAME = "AUTH_OIDC_NAME";
	const AUTH_OIDC_URL = "AUTH_OIDC_URL";
	const AUTH_OIDC_CLIENT_ID = "AUTH_OIDC_CLIENT_ID";
	const AUTH_OIDC_CLIENT_SECRET = "AUTH_OIDC_CLIENT_SECRET";
	const AUTH_OIDC_CLIENT_USERNAME_CLAIM = "AUTH_OIDC_CLIENT_USERNAME_CLAIM";

	/** duration (seconds), -1 disables */
	const AUTH_OIDC_VALIDATE_INTERVAL = "AUTH_OIDC_VALIDATE_INTERVAL";

	/** @var PluginHost $host */
	private $host;

	function about() {
		return array(null,
			"Authenticates against configured OIDC provider",
			"fox",
			true);
	}

	function init($host) {
		Config::add(self::AUTH_OIDC_POST_LOGOUT_URL, "", Config::T_STRING);
		Config::add(self::AUTH_OIDC_NAME, "", Config::T_STRING);
		Config::add(self::AUTH_OIDC_URL, "", Config::T_STRING);
		Config::add(self::AUTH_OIDC_CLIENT_ID, "", Config::T_STRING);
		Config::add(self::AUTH_OIDC_CLIENT_SECRET, "", Config::T_STRING);
		Config::add(self::AUTH_OIDC_VALIDATE_INTERVAL, "-1", Config::T_INT);
		Config::add(self::AUTH_OIDC_CLIENT_USERNAME_CLAIM, "preferred_username", Config::T_STRING);

		if (Config::get(self::AUTH_OIDC_URL)) {
			$host->add_hook($host::HOOK_AUTH_USER, $this);
			$host->add_hook($host::HOOK_LOGINFORM_ADDITIONAL_BUTTONS, $this);

			if (Config::get(self::AUTH_OIDC_VALIDATE_INTERVAL) > 0)
				$host->add_hook($host::HOOK_VALIDATE_SESSION, $this);

			if (Config::get(self::AUTH_OIDC_POST_LOGOUT_URL) != "")
				$host->add_hook($host::HOOK_POST_LOGOUT, $this);
		}

		$this->host = $host;
	}

	function hook_validate_session(): bool {
		$access_token = $_SESSION["auth_oidc:access_token"] ?? null;

		// only try to validate our (oidc) sessions with access token stored
		if ($access_token && Config::get(self::AUTH_OIDC_VALIDATE_INTERVAL) > 0 && $_SESSION["auth_oidc:access_token_last_check"] < time() - Config::get(self::AUTH_OIDC_VALIDATE_INTERVAL)) {

			$oidc = new OpenIDConnectClient(Config::get(self::AUTH_OIDC_URL),
						Config::get(self::AUTH_OIDC_CLIENT_ID),
						Config::get(self::AUTH_OIDC_CLIENT_SECRET));

			try {
				$result = $oidc->introspectToken($access_token);

				if ($result->active)
						$_SESSION["auth_oidc:access_token_last_check"] = time();

				return $result->active;
			} catch (Exception $e) {
				$_SESSION["login_error_msg"] = 'OIDC: ' . $e->getMessage();
				return false;
			}
		}

		return true;
	}

	function is_public_method($method) {
		return $method == "oidc_login";
	}

	public function oidc_login() : void {
		$oidc = new OpenIDConnectClient(Config::get(self::AUTH_OIDC_URL),
			Config::get(self::AUTH_OIDC_CLIENT_ID),
			Config::get(self::AUTH_OIDC_CLIENT_SECRET));

		$oidc->setRedirectURL(Config::get_self_url());
		$oidc->addScope(['openid', 'profile', 'email']);
		$oidc->authenticate();
	}

	function authenticate($login, $password, $service = '') {
		if (!($_SESSION['uid'] ?? false) && ($_REQUEST['code'] ?? false)) {

			$oidc = new OpenIDConnectClient(Config::get(self::AUTH_OIDC_URL),
				Config::get(self::AUTH_OIDC_CLIENT_ID),
				Config::get(self::AUTH_OIDC_CLIENT_SECRET));

			try {
				$oidc->setRedirectURL(Config::get_self_url());
				$oidc->addScope(['openid', 'profile', 'email']);
				$oidc->authenticate();

				$login = mb_strtolower($oidc->requestUserInfo(Config::get(self::AUTH_OIDC_CLIENT_USERNAME_CLAIM)));

				$user_id = $this->auto_create_user($login, $password);

				if ($user_id) {
					$name = $oidc->requestUserInfo("name");

					if ($name) {
						$sth = $this->pdo->prepare("UPDATE ttrss_users SET full_name = ? WHERE id = ?");
						$sth->execute([$name, $user_id]);
					}

					$email = $oidc->requestUserInfo("email");

					if ($email) {
						$sth = $this->pdo->prepare("UPDATE ttrss_users SET email = ? WHERE id = ?");
						$sth->execute([$email, $user_id]);
					}
				}

				if (Config::get(self::AUTH_OIDC_VALIDATE_INTERVAL) > 0) {
					$access_token = $oidc->getAccessToken();

					$_SESSION["auth_oidc:access_token"] = $access_token;
					$_SESSION["auth_oidc:access_token_last_check"] = time();
				}

				return $user_id;

			} catch (Exception $e) {
				$_SESSION["login_error_msg"] = 'OIDC: ' . $e->getMessage();
			}
		}

		return false;
	}

	function get_login_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function hook_loginform_additional_buttons() {
		print \Controls\button_tag(T_sprintf('Log in with %s', Config::get(self::AUTH_OIDC_NAME)), '',
			['class' => '', 'onclick' => 'Plugins.Auth_OIDC.login("'.htmlspecialchars($this->host->get_public_method_url($this, "oidc_login")).'")']);
	}

	function hook_post_logout($login, $user_id) {
		return [
			Config::get(self::AUTH_OIDC_POST_LOGOUT_URL)
			];
	}

	function api_version() {
		return 2;
	}

}
