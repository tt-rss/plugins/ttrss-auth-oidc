require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	ready(function() {
		Plugins.Auth_OIDC = {
			login: function(url) {
				window.location.href = url;
			}
		}
	})
});
